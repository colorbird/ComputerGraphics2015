/*************************************************************************
    CSC418/2504, Winter 20l5
    Assignment 1

    Instructions:
        See main.cpp for more instructions.

        This file contains the class OpenGL portion of the main window.
**************************************************************************/

#include "GLWidget.h"
#include <iostream>

static QGLFormat createQGLFormat()
{
    // Return a QGLFormat object that tells Qt information about what
    // OpenGL context we would like.
    QGLFormat format(QGL::SampleBuffers);
    if (supportsNewOpenGL())
    {
	// Choose a version of OpenGL that supports vertex array objects and
	// tell it that we do not want support for deprecated functions.
        format.setVersion(3, 3);
        format.setProfile(QGLFormat::CoreProfile);
    }
    return format;
}

GLWidget::GLWidget(QWidget *parent)
    : QGLWidget(createQGLFormat(), parent)
    , m_is_animating(false)
    , m_animation_frame(0)
    , m_joint_angle(0)
    , m_arm_angle(0)
    , m_left_leg_angle(0)
    , m_left_foot_angle(0)
    , m_right_leg_angle(0)
    , m_right_foot_angle(0)
    , m_penguin_mouth(0)
    , m_leg_translate(0)
    , m_whole_penguin_translate(0)
{
    // Start a timer that will call the timerEvent method every 50ms.
    startTimer(/*milliseconds=*/50);
}

void GLWidget::initializeGL()
{
    m_gl_state.initializeGL();

    // To aid with troubleshooting, print out which version of OpenGL we've
    // told the driver to use.
    std::cout << "Using OpenGL: " << glGetString(GL_VERSION) << std::endl;


    // Copy the data for the shapes we'll draw into the video card's memory.
    m_penguin_leg.initialize(m_gl_state.VERTEX_POSITION_SHADER_LOCATION);
    m_penguin_foot.initialize(m_gl_state.VERTEX_POSITION_SHADER_LOCATION);
    m_penguin_arm.initialize(m_gl_state.VERTEX_POSITION_SHADER_LOCATION);
    m_penguin_belly.initialize(m_gl_state.VERTEX_POSITION_SHADER_LOCATION);
    m_penguin_lower_mouth.initialize(m_gl_state.VERTEX_POSITION_SHADER_LOCATION);
    m_penguin_upper_mouth.initialize(m_gl_state.VERTEX_POSITION_SHADER_LOCATION);
    m_penguin_face.initialize(m_gl_state.VERTEX_POSITION_SHADER_LOCATION);
    m_unit_square.initialize(m_gl_state.VERTEX_POSITION_SHADER_LOCATION);
    m_unit_circle.initialize(
	m_gl_state.VERTEX_POSITION_SHADER_LOCATION,
	/*num_circle_segments=*/360);

    // Tell OpenGL what color to fill the background to when clearing.
    glClearColor(/*red=*/0.7f, /*green=*/0.7f, /*blue=*/0.9f, /*alpha=*/1.0f);

}

void GLWidget::resizeGL(int width, int height)
{
    // Respond to the window being resized by updating the viewport and
    // projection matrices.

    checkForGLErrors();

    // Setup projection matrix for new window
    m_gl_state.setOrthographicProjectionFromWidthAndHeight(width, height);

    // Update OpenGL viewport and internal variables
    glViewport(0, 0, width, height);
    checkForGLErrors();
}

void GLWidget::timerEvent(QTimerEvent *)
{
    // Respond to a timer going off telling us to update the animation.
    if (!m_is_animating)
        return;

    // increment the frame number.
    m_animation_frame++;

    // Update joint angles.
    const double joint_rot_speed = 0.1;
    double joint_rot_t =
        (sin(m_animation_frame * joint_rot_speed) + 1.0) / 2.0;
    m_joint_angle = joint_rot_t * JOINT_MIN + (1 - joint_rot_t) * JOINT_MAX;

    //////////////////////////////////////////////////////////////////////////
    // TODO:
    //   Modify this function to animate the character's joints
    //   Note: Nothing should be drawn in this function!
    m_whole_penguin_translate=(joint_rot_t * PENGUIN_MIN + (1 - joint_rot_t) * PENGUIN_MAX)/PENGUIN_MULTIPLIER;  
    m_penguin_mouth=(joint_rot_t * MOUTH_MIN + (1 - joint_rot_t) * MOUTH_MAX)/MOUTH_MULTIPLIER;      
    m_arm_angle = joint_rot_t * ARM_MIN + (1 - joint_rot_t) * ARM_MAX;
    m_left_leg_angle = joint_rot_t * LEFT_LEG_MIN + (1 - joint_rot_t) * LEFT_LEG_MAX;
    m_left_foot_angle = joint_rot_t * LEFT_FOOT_MIN + (1 - joint_rot_t) * LEFT_FOOT_MAX;
    m_right_leg_angle = joint_rot_t * RIGHT_LEG_MIN + (1 - joint_rot_t) * RIGHT_LEG_MAX;
    m_right_foot_angle = joint_rot_t * RIGHT_FOOT_MIN + (1 - joint_rot_t) * RIGHT_FOOT_MAX;
    m_leg_translate=(joint_rot_t * MOUTH_MIN + (1 - joint_rot_t) * MOUTH_MAX)/LEG_TRANSLATE_MULTIPLIER; 
    //////////////////////////////////////////////////////////////////////////

    // Tell this widget to redraw itself.
    update();
}

void GLWidget::paintGL()
{
    // This method gets called by the event handler to draw the scene, so
    // this is where you need to build your scene -- make your changes and
    // additions here.  All rendering happens in this function.

    checkForGLErrors();

    // Clear the screen with the background colour.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  

    // Setup the model-view transformation matrix stack.
    transformStack().loadIdentity();
    checkForGLErrors();

    //////////////////////////////////////////////////////////////////////////
    // TODO:
    //   Modify this function draw the scene.  This should include function
    //   calls to pieces that apply the appropriate transformation matrices
    //   and render the individual body parts.
    //////////////////////////////////////////////////////////////////////////

    // Draw our penguin
    // penguin belly
    const float BODY_WIDTH = 40.0f;
    const float BODY_LENGTH = 40.0f;
    
    //penguin belly upside down
    const double belly_angle=180;

    // Note that successive transformations are applied *before* the previous
    // ones.

    // Push the current transformation matrix on the stack
    transformStack().pushMatrix();
 transformStack().pushMatrix();
//translate the whole penguin
 transformStack().translate(-m_whole_penguin_translate*900,0);
  
        // Draw the right 'arm'
        transformStack().pushMatrix();
      
           //angle the arms slightly
           transformStack().rotateInDegrees(-(-m_arm_angle+75));   
            // Scale to the size of the arms
	  transformStack().scale(BODY_WIDTH/2, BODY_LENGTH);
            // Translate to top of the arms
          transformStack().translate(0,-1.7);
           // Set the colour to green
            m_gl_state.setColor(0.27,0.27,0.27);
            m_penguin_arm.draw();
                 // Draw the 'spinner of arm'
        	transformStack().pushMatrix();
         	   // Scale to the size of the spinner
	  	transformStack().scale(0.2, 0.2);
            	 // Translate to top of the spinner
          	transformStack().translate(0.0,6.5);
           	 // Set the colour to black
            	m_gl_state.setColor(0.0, 0.0, 0.0);
            	m_unit_circle.draw();
        	transformStack().popMatrix();
        transformStack().popMatrix();         

        // Draw the 'belly'
        transformStack().pushMatrix();
            // Scale to the size of the belly
	transformStack().scale(BODY_WIDTH, 3*BODY_LENGTH/2);
         // make it upside down
        transformStack().rotateInDegrees(belly_angle-10+m_joint_angle/10);
            // Set the colour to green
        m_gl_state.setColor(0.35,.35,0.35);
        m_penguin_belly.draw();
        transformStack().popMatrix();


         // Draw the 'head'
        transformStack().pushMatrix();
            // Scale to the size of the head
	transformStack().scale(BODY_WIDTH, 3*BODY_LENGTH/2);
              // Translate to top of the body
        transformStack().translate(0,1.5);
            // Set the colour to green
        m_gl_state.setColor(0.50,.50,0.50);
        m_penguin_face.draw();
         	// Draw the 'spinner of head/belly'
        	transformStack().pushMatrix();
         	   // Scale to the size of the head
	  	transformStack().scale(0.10, 0.10);    
           	 // Translate to top of the body
          	transformStack().translate(0.0,-7.6);
           	 // Set the colour to green
            	m_gl_state.setColor(0.15,0.15,0.15);
            	m_unit_circle.draw();
        	transformStack().popMatrix(); 
        transformStack().popMatrix(); 
        
          // Draw the 'outer eye'
        transformStack().pushMatrix();
            // Scale to the size of the eye
	  transformStack().scale(BODY_WIDTH/10, BODY_LENGTH/10);
             // Translate to top of the eye
          transformStack().translate(-4.5,20.7);
            // Set the colour to green
            m_gl_state.setColor(1.0, 1.0, 1.0);

            m_unit_circle.draw();
        transformStack().popMatrix(); 
       
          // Draw the 'inner eye'
        transformStack().pushMatrix();
            // Scale to the size of the eye
	  transformStack().scale(BODY_WIDTH/14, BODY_LENGTH/14);
           
            // Translate to top of the eye
          transformStack().translate(-6.3,29.0);
            // Set the colour to green
            m_gl_state.setColor(0.0, 0.0, 0.0);

            m_unit_circle.draw();
        transformStack().popMatrix(); 
    
 
  // Draw the 'lowermouth'
        transformStack().pushMatrix();
            // Scale to the size of the mouth
	  transformStack().scale(BODY_WIDTH/2, BODY_LENGTH/2);
           // Translate to top of the mouth
          transformStack().translate(-2.2,3.3);
            // Set the colour to red
            m_gl_state.setColor(0.27,0.27,0.27);
            m_penguin_lower_mouth.draw();
        transformStack().popMatrix(); 

   // Draw the 'uppermouth'
        transformStack().pushMatrix();
            // Scale to the size of the mouth
	  transformStack().scale(1*BODY_WIDTH/2, 3*BODY_LENGTH/2);
            // Translate to top of the mouth
          transformStack().translate(-2.2,1.8+ m_penguin_mouth/4);
            // Set the colour to green
            m_gl_state.setColor(0.27,0.27,0.27);
            m_penguin_upper_mouth.draw();
        transformStack().popMatrix(); 

   // Draw the left 'arm'
        transformStack().pushMatrix();
      
           //angle the arms slightly
           transformStack().rotateInDegrees(-m_arm_angle+75);   
            // Scale to the size of the arms
	  transformStack().scale(BODY_WIDTH/2, BODY_LENGTH);
            // Translate to top of the arms
          transformStack().translate(0,-1.7);
           // Set the colour 
            m_gl_state.setColor(0.27,0.27,0.27);
            m_penguin_arm.draw();
                 // Draw the 'spinner of arm'
        	transformStack().pushMatrix();
         	   // Scale to the size of the spinner
	  	transformStack().scale(0.2, 0.2);
            	 // Translate to top of the spinner
          	transformStack().translate(0.0,6.5);
           	 // Set the colour to green
            	m_gl_state.setColor(0.15,0.15,0.15);
            	m_unit_circle.draw();
        	transformStack().popMatrix();
        transformStack().popMatrix(); 


      // Draw the screen left'leg'
       transformStack().pushMatrix();
            // Scale 
       transformStack().scale(BODY_WIDTH/4, BODY_LENGTH/2); 
            // Translate to top 
       transformStack().translate( -2+m_leg_translate/2,-9.4);
           //angle the legs slightly
       transformStack().rotateInDegrees(30+m_left_leg_angle);
            // Set the colour to green
       m_gl_state.setColor(0.27,0.27,0.27);
       m_penguin_leg.draw();
     
		 // Draw the 'spinner of right leg'
        	transformStack().pushMatrix();
               // transformStack().rotateInDegrees(-5);
                // Translate to top of the body
          	transformStack().translate(0.8,-0.1);
       	        // Scale to the size of the head
	  	transformStack().scale(0.2, 0.2);
                // Set the colour to green
            	m_gl_state.setColor(0.15,0.15,0.15);
            	m_unit_circle.draw();
        	transformStack().popMatrix();

            // Draw the screen right'foot'
          transformStack().pushMatrix();
            // Scale to the size of the head
	               // Translate to top of the body
          transformStack().translate(-0.1,0.2);    
          transformStack().rotateInDegrees(-90+m_left_foot_angle);
            // Set the colour to green
          m_gl_state.setColor(0.27,0.27,0.27);
          m_penguin_foot.draw();
   
                // Draw the 'spinner of right foot'
        	transformStack().pushMatrix();
               // transformStack().rotateInDegrees(-5);
                // Translate 
          	transformStack().translate(0.8,-0.4);
       	        // Scale to the size of the foot
	  	transformStack().scale(0.4, 0.2);
                // Set the colour 
            	m_gl_state.setColor(0.15,0.15,0.15);
            	m_unit_circle.draw();
        	transformStack().popMatrix();


        transformStack().popMatrix();  
     transformStack().popMatrix(); 


      // Draw the screen right'leg'
       transformStack().pushMatrix();
            // Scale to the size of the head
       transformStack().scale(BODY_WIDTH/4, BODY_LENGTH/2); 
            // Translate to top of the body
       transformStack().translate(-  m_leg_translate/2,-9.4);
           //angle the legs slightly
       transformStack().rotateInDegrees(30+m_right_leg_angle);
            // Set the colour to green
       m_gl_state.setColor(0.27,0.27,0.27);
       m_penguin_leg.draw();
     
		 // Draw the 'spinner of right leg'
        	transformStack().pushMatrix();
               // transformStack().rotateInDegrees(-5);
                // Translate to top of the body
          	transformStack().translate(0.8,-0.1);
       	        // Scale to the size of the head
	  	transformStack().scale(0.2, 0.2);
                // Set the colour to green
            	m_gl_state.setColor(0.15,0.15,0.15);
            	m_unit_circle.draw();
        	transformStack().popMatrix();

            // Draw the screen right'foot'
          transformStack().pushMatrix();
            // Scale to the size of the head
	   // transformStack().scale(BODY_WIDTH/4, BODY_LENGTH/2);
            // Translate to top of the body
          transformStack().translate(-0.1,0.2);//1-  m_leg_translate/2,0);      
          transformStack().rotateInDegrees(-90+m_right_foot_angle);//m_right_foot_angle);
            // Set the colour to green
          m_gl_state.setColor(0.27,0.27,0.27);
          m_penguin_foot.draw();
   
                // Draw the 'spinner of right foot'
        	transformStack().pushMatrix();
               // transformStack().rotateInDegrees(-5);
                // Translate 
          	transformStack().translate(0.8,-0.4);
       	        // Scale to the size of the foot
	  	transformStack().scale(0.4, 0.2);
                // Set the colour 
            	m_gl_state.setColor(0.15,0.15,0.15);
            	m_unit_circle.draw();
        	transformStack().popMatrix();


        transformStack().popMatrix();  
     transformStack().popMatrix(); 

     

transformStack().popMatrix();
      
    // Retrieve the previous state of the transformation stack
    transformStack().popMatrix();


    // Execute any GL functions that are in the queue just to be safe
    glFlush();
    checkForGLErrors();
}

